import Vue from 'vue'
import App from './App.vue'
import router from './router'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import { library } from '@fortawesome/fontawesome-svg-core'
import {
  faUserSecret,
  faArrowLeft,
  faArrowRight
} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import Vuelidate from 'vuelidate'
import VueRouter from 'vue-router'

Vue.config.productionTip = false
// Make BootstrapVue available throughout your project
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)
library.add(faUserSecret)
library.add(faArrowLeft)
library.add(faArrowRight)
Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.use(Vuelidate)
Vue.use(VueRouter)

const MyPlugin = {
  install (Vue) {
    Vue.prototype.getBaseURL = () => {
      var baseUrl = 'http://127.0.0.1:5000/api'
      return baseUrl
    }
  }
}
Vue.use(MyPlugin)

Vue.prototype.$apiUrls = {
  root: 'http://127.0.0.1:5000/api',
  api: 'api/v1/'
}

new Vue({
  router: router,
  render: h => h(App)
}).$mount('#app')
