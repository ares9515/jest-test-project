import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import NewOrder from './views/NewOrder.vue'
import EditOrder from './views/EditOrder.vue'
import PrintInvoice from './views/PrintInvoice.vue'
import PrintNewRetails from './views/PrintNewRetails.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/neworder',
      name: 'neworder',
      component: NewOrder
    },
    {
      path: '/editorder/:id',
      name: 'editorder',
      component: EditOrder
    },
    {
      path: '/printinvoice/:ids',
      name: 'printinvoice',
      component: PrintInvoice
    },
    {
      path: '/print/new/retails',
      name: 'printnewinvoice',
      component: PrintNewRetails
    }
  ]
})
